const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql2');

const app = express();

app.use(bodyparser.json());
app.use(cors("*"));

var mysqlConnection = mysql.createConnection({
    host:'host.docker.internal',//host.docker.internal
    port:6000,
    user:'root',
    password:'root',
    database:'ali'
});

mysqlConnection.connect((err)=>{
    if(!err){
        console.log("Connection to database succesfull");
    }else{
        console.log("database connection failed");
    }
});

app.get("/", (req,res)=>{
    res.send("Welcome to moviedb");
});


//read operation
app.get("/read", (req,res)=>{
    mysqlConnection.query("select * from Movie_tb", (err, result)=>{
        if(!err){
            res.send(result);
        }else{
            console.log(err);
        }
    });
});

//read operation with id
app.get("/read/:id", (req,res)=>{
    mysqlConnection.query("select * from Movie_tb where movie_id=?",[req.params.id], (err, result)=>{
        if(!err){
            res.send(result);
        }else{
            console.log(err);
        }
    });
});

//insert operation
app.post("/read", (req,res)=>{
    var values = req.body;
    mysqlConnection.query("insert into Movie_tb values(?, ?, ?, ?, ?)", [values.movie_id, values.movie_title, values.movie_release_date, values.movie_time, values.director_name],(err, result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    });
});

//delete operation with id
app.delete("/read/:id", (req,res)=>{
    mysqlConnection.query("delete from Movie_tb where movie_id=?",[req.params.id], (err, result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    });
});


app.listen(3000,()=>{
    console.log("Express listening");
});





